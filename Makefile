NAME = _code

CC = g++
CFLAGS = -Wall -Wextra -Werror -Wno-unused-result -std=c++17 -O2
 
SRC_DIR = src/
SRC_DIRS := $(shell find $(SRC_DIR) -type d)
SRCS := $(shell find $(SRC_DIR) -type f -name "*.cpp")

HDRS := $(shell find $(SRC_DIR) -type f -name "*.hpp")

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(SRC_DIRS))
OBJS := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRCS))

all: $(NAME) tags

$(NAME): $(OBJ_DIRS) $(OBJS)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDRS)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIRS): $(SRC_DIRS)
	mkdir -p $(OBJ_DIRS)

tags: $(SRCS) $(HDRS)
	ctags -R src/

clean:
	rm -rf $(OBJ_DIRS)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
