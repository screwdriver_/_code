#ifndef _CODE_HPP
# define _CODE_HPP

# include <iostream>
# include <string>
# include <string.h>
# include <vector>

# define DEFAULT_ACTION	ALL

namespace _code
{
	enum flags : char
	{
		ASSEMBLY = 'a',
		NAME = 'n',
		WARN = 'w',
		OPTIMIZE = 'O',
		DEBUG = 'd',

		VOID = '\0'
	};

	struct param
	{
		char flag;
		std::string value;
	};

	enum ast_type
	{
		ROOT = 0,
		INCLUDE,
		NAMESPACE,
		TYPEDEF,
		VARIABLE,
		FUNCTION,
		STRUCT,
		ENUM,
		TEMPLATE,
		IF,
		ELSE,
		SWITCH,
		CASE,
		WHILE,
		FOR,
		DO,
		BREAK,
		CONTINUE,
		TRY,
		CATCH,
		THROW,
		TERNARY,
		RETURN,
		ASM
	};

	struct ast
	{
		ast_type type;

		std::vector<ast> children;
		std::vector<ast> scope;

		inline ast(const ast_type type)
			: type{type}
		{}
	};

	struct ast_root : public ast
	{
		inline ast_root()
			: ast(ROOT)
		{}
	};

	struct ast_include : public ast
	{
		inline ast_include()
			: ast(INCLUDE)
		{}
	};

	struct ast_namespace : public ast
	{
		inline ast_namespace()
			: ast(NAMESPACE)
		{}
	};

	struct ast_typedef : public ast
	{
		inline ast_typedef()
			: ast(TYPEDEF)
		{}
	};

	struct ast_variable : public ast
	{
		inline ast_variable()
			: ast(VARIABLE)
		{}
	};

	struct ast_function : public ast
	{
		inline ast_function()
			: ast(FUNCTION)
		{}
	};

	struct ast_struct : public ast
	{
		inline ast_struct()
			: ast(STRUCT)
		{}
	};

	struct ast_enum : public ast
	{
		inline ast_enum()
			: ast(ENUM)
		{}
	};

	struct ast_template : public ast
	{
		inline ast_template()
			: ast(TEMPLATE)
		{}
	};

	struct ast_if : public ast
	{
		inline ast_if()
			: ast(IF)
		{}
	};

	struct ast_else : public ast
	{
		inline ast_else()
			: ast(ELSE)
		{}
	};

	struct ast_switch : public ast
	{
		inline ast_switch()
			: ast(SWITCH)
		{}
	};

	struct ast_case : public ast
	{
		inline ast_case()
			: ast(CASE)
		{}
	};

	struct ast_while : public ast
	{
		inline ast_while()
			: ast(WHILE)
		{}
	};

	struct ast_for : public ast
	{
		inline ast_for()
			: ast(FOR)
		{}
	};

	struct ast_do : public ast
	{
		inline ast_do()
			: ast(DO)
		{}
	};

	struct ast_break : public ast
	{
		inline ast_break()
			: ast(BREAK)
		{}
	};

	struct ast_continue : public ast
	{
		inline ast_continue()
			: ast(CONTINUE)
		{}
	};

	struct ast_try : public ast
	{
		inline ast_try()
			: ast(TRY)
		{}
	};

	struct ast_catch : public ast
	{
		inline ast_catch()
			: ast(CATCH)
		{}
	};

	struct ast_throw : public ast
	{
		inline ast_throw()
			: ast(THROW)
		{}
	};

	struct ast_ternary : public ast
	{
		inline ast_ternary()
			: ast(TERNARY)
		{}
	};

	struct ast_return : public ast
	{
		inline ast_return()
			: ast(RETURN)
		{}
	};

	struct ast_asm : public ast
	{
		inline ast_asm()
			: ast(ASM)
		{}
	};

	enum error_type
	{
		WARNING = 0,
		ERROR,
		FATAL
	};

	std::vector<param> parse_params(char **args);
	const param *get_param(const std::vector<param> &params, const char flag);
	std::vector<param>::const_iterator
		get_files_begin(const std::vector<param> &params);

	ast_root parse(const std::vector<param> &params,
		const std::vector<param> &files);

	void compile(const std::vector<param> &params, const ast_root &ast);

	void error(const error_type type, const std::string &message);
}

#endif
