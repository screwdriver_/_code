#include "_code.hpp"

using namespace std;

int main(int argc, char **argv)
{
	(void) argc;
	using namespace _code;

	const auto p = parse_params(argv + 1);
	// TODO Check if valid params

	const auto files_begin = get_files_begin(p);

	if(files_begin == p.cend()) error(FATAL, "no input files");

	const vector<param> params(p.cbegin(), files_begin);
	const vector<param> files(files_begin, p.cend());

	const auto ast = parse(params, files);
	compile(params, ast);

	return 0;
}
