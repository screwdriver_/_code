#include "_code.hpp"

using namespace _code;

void _code::error(const error_type type, const std::string &message)
{
	std::cout << "_code: ";

	switch(type)
	{
		case WARNING:
		{
			std::cout << "\033[35mwarning";
			break;
		}

		case ERROR:
		{
			std::cout << "\033[91merror";
			break;
		}

		case FATAL:
		{
			std::cout << "\033[31mfatal error";
			break;
		}
	}

	std::cout << "\033[0m: " << message << '\n';

	if(type == FATAL) exit(-1);
}
