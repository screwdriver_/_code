#include "_code.hpp"

using namespace _code;

std::vector<param> _code::parse_params(char **args)
{
	std::vector<param> params;

	while(*args)
	{
		if(std::string(*args) == "--")
		{
			++args;
			break;
		}

		if(strlen(*args) != 2 || **args != '-') break;

		params.emplace_back();
		auto &p = params.back();
		p.flag = (*args)[1];

		if(!*(args + 1)) break;

		p.value = *(args + 1);
		++args;
	}

	while(*args)
	{
		params.emplace_back();
		auto &p = params.back();
		p.flag = VOID;

		p.value = *args;
		++args;
	}

	return params;
}

const param *_code::get_param(const std::vector<param> &params, const char flag)
{
	for(const auto &p : params)
	{
		if(p.flag == flag) return &p;
	}

	return nullptr;
}

std::vector<param>::const_iterator
	_code::get_files_begin(const std::vector<param> &params)
{
	auto i = params.cbegin();

	for(; i != params.cend(); ++i)
	{
		if(i->flag == VOID) break;
	}

	return i;
}
